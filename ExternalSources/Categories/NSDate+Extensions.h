//
//  NSDate+Extensions.h
//  Twitzer
//
//  Created by Abdul Rehman on 13/01/2013.
//  Copyright (c) 2013 InnovationNext. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extensions)

- (BOOL)isToday;

@end
