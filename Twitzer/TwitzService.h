//
//  TwitzService.h
//  Twitzer
//
//
//  Twitzer backend service.
//
//  |TwitzerServiceManager| is responsiable for downloading and parsing
//  data from server.
//
//  |TwitzInfo| a model object representing a single Twitz object
//
//
//  Created by Abdul Rehman on 13/01/2013.
//  Copyright (c) 2013 InnovationNext. All rights reserved.
//



#ifndef Twitzer_TwitzService_h
#define Twitzer_TwitzService_h

#import "TwitzerServiceManager.h"
#import "TwitzInfo.h"

#endif
