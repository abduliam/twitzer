//
//  TwitzListViewController.m
//  Twitzer
//
//  Created by Abdul Rehman on 13/01/2013.
//  Copyright (c) 2013 InnovationNext. All rights reserved.
//

#import "TwitzListViewController.h"
#import "TwitzService.h"
#import "MBProgressHUD.h"
#import "NSDate+Extensions.h"

#define TAG_AV_RETRY        4040

@interface TwitzListViewController () {
    NSMutableArray *_twitzObjects;
}

@property(nonatomic,retain)MBProgressHUD *progressHUD;
@property(nonatomic,retain)NSDateFormatter *twitzDateFormatter;
@property(nonatomic,retain)UIFont *twitzTextFont;

- (void)actionRefresh:(id)sender;

@end

@implementation TwitzListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Twitzer", @"Twitzer");
    }
    return self;
}

- (void)dealloc {
    [_twitzObjects release];
    [_contentTV release];
    [_progressHUD release];
    [_twitzDateFormatter release];
    [super dealloc];
}

#pragma mark - UIViewController lifecycle
- (void)viewDidUnload {
    [self setContentTV:nil];
    [self setProgressHUD:nil];
    [self setTwitzDateFormatter:nil];
    [super viewDidUnload];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *refreshButton = [[[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                       target:self
                                       action:@selector(actionRefresh:)] autorelease];
    self.navigationItem.rightBarButtonItem = refreshButton;
    
    self.twitzDateFormatter=[[[NSDateFormatter alloc] init] autorelease];
    [self.twitzDateFormatter setDateFormat:@"MMM dd"];
    self.twitzTextFont=[UIFont systemFontOfSize:17];
    
    [self actionRefresh:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - User defined methods
#pragma mark - Private methods
- (void)actionRefresh:(id)sender {
    if (!_twitzObjects) {
        _twitzObjects = [[NSMutableArray alloc] init];
    }
    
    self.progressHUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.progressHUD removeFromSuperViewOnHide];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    [TwitzerServiceManager retrieveTwitzWithProgress:^(float progress) {
        if (self.progressHUD.mode!=MBProgressHUDModeAnnularDeterminate) {
            self.progressHUD.mode=MBProgressHUDModeAnnularDeterminate;
        }
        [self.progressHUD setProgress:progress];
     } success:^(NSArray *listOftwitz) {
         NSSortDescriptor *sortDescriptor;
         sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"date"
                                                       ascending:NO] autorelease];
         NSArray *sortDescriptors = @[sortDescriptor];
         
         NSArray *sorted = [listOftwitz sortedArrayUsingDescriptors:sortDescriptors];
         
         [_twitzObjects removeAllObjects];
         [_twitzObjects addObjectsFromArray:sorted];
         [self.contentTV reloadData];
         [self.progressHUD hide:YES];
         self.progressHUD=nil;
         [self.navigationItem.rightBarButtonItem setEnabled:YES];
     } failure:^(NSError *err, NSString *localizedReason) {
         UIAlertView *avRetry=[[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"Unable to Download!", @"")
                               message:[NSString stringWithFormat:NSLocalizedString(@"Can not download latest twitx (%@), Do you want to retry?", @""), localizedReason]
                               delegate:self
                               cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancle")
                               otherButtonTitles:NSLocalizedString(@"Retry", @"Retry"), nil];
         avRetry.tag=TAG_AV_RETRY;
         [avRetry show];
         [avRetry release];
         [self.progressHUD hide:YES];
         self.progressHUD=nil;
         [self.navigationItem.rightBarButtonItem setEnabled:YES];
     }];
}


#pragma mark - Delegate Impl
#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _twitzObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                       reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.numberOfLines=0;
        cell.textLabel.font=self.twitzTextFont;
    }
    
    TwitzInfo *twitz=[_twitzObjects objectAtIndex:indexPath.row];
    cell.textLabel.text = twitz.text;
    NSString *dateStr=nil;
    if ([twitz.date isToday]) {
        dateStr=NSLocalizedString(@"today", @"today");
    }else{
        dateStr=[self.twitzDateFormatter stringFromDate:twitz.date];
    }
    
    cell.detailTextLabel.text=[NSString stringWithFormat:@"%@, %@", twitz.username, dateStr];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    TwitzInfo *twitz=[_twitzObjects objectAtIndex:indexPath.row];
    
    CGSize constraint = CGSizeMake(300, 999);
    CGSize size = [twitz.text sizeWithFont:self.twitzTextFont constrainedToSize:constraint lineBreakMode:UILineBreakModeWordWrap];

    
    CGFloat height = MAX(size.height, self.contentTV.rowHeight);
    return height + 40;//padding 20px top & bottom
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag==TAG_AV_RETRY) {
        if (buttonIndex==1) {
            [self actionRefresh:nil];
        }
    }
}

@end
