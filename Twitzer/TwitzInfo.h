//
//  TwitzInfo.h
//  Twitzer
//
//  Created by Abdul Rehman on 13/01/2013.
//  Copyright (c) 2013 InnovationNext. All rights reserved.
//

#import <Foundation/Foundation.h>

//
// A sample model class for storing data related to a Twitz
//
@interface TwitzInfo : NSObject

@property(retain,nonatomic)NSString *username;
@property(retain,nonatomic)NSString *text;
@property(retain,nonatomic)NSDate *date;



@end
