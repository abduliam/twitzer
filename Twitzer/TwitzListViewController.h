//
//  TwitzListViewController.h
//  Twitzer
//
//  Main UIViewController subclass to display list of tweetz, implemented using
//  a UITableView incorporating cells with type UITableViewCellStyleSubtitle
//
//
//  Created by Abdul Rehman on 13/01/2013.
//  Copyright (c) 2013 InnovationNext. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwitzListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

// UITableView loaded for displaying tweetz
@property (retain, nonatomic) IBOutlet UITableView *contentTV;

@end
