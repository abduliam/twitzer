//
//  TwitzInfo.m
//  Twitzer
//
//  Created by Abdul Rehman on 13/01/2013.
//  Copyright (c) 2013 InnovationNext. All rights reserved.
//

#import "TwitzInfo.h"

@implementation TwitzInfo

-(void)dealloc {
    [_username release];
    [_text release];
    [_date release];
    [super dealloc];
}

@end
