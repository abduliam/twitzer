//
//  TwitzerServiceManager.h
//  Twitzer
//
//  Created by Abdul Rehman on 13/01/2013.
//  Copyright (c) 2013 InnovationNext. All rights reserved.
//

#import <Foundation/Foundation.h>

// Base url of the Twitzer server
#define SERVER_WS_URL                 [NSURL URLWithString:@"https://twitzer-api.herokuapp.com/"]


// A sample model class for storing data related to a Twitz
@interface TwitzerServiceManager : NSObject

// Retrives latest tweetzs from server, updates the download progress
// using |progress| block, in case of success an Array of |TwitzInfo| is
// returned in |success| block, if something goes wrong an error with
// appropriate error reason is returned in |failure| block.
// see |TwitzInfo|
+ (void)retrieveTwitzWithProgress:(void (^)(float))progress
                          success:(void (^)(NSArray *listOftwitz))success
                          failure:(void (^)(NSError *error, NSString *loclizedReason))failure;
@end
