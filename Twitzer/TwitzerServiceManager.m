//
//  TwitzerServiceManager.m
//  Twitzer
//
//  Created by Abdul Rehman on 13/01/2013.
//  Copyright (c) 2013 InnovationNext. All rights reserved.
//

#import "TwitzerServiceManager.h"
#import "ASIHTTPRequest.h"
#import "SBJson.h"
#import "TwitzInfo.h"

@implementation TwitzerServiceManager

+ (void)retrieveTwitzWithProgress:(void (^)(float))progress
                          success:(void (^)(NSArray *listOftwitz))success
                          failure:(void (^)(NSError *error, NSString *loclizedReason))failure; {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *requestURL=[NSURL URLWithString:@"timeline" relativeToURL:SERVER_WS_URL];
        ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[requestURL absoluteURL]];
        [request setBytesReceivedBlock:^(unsigned long long size, unsigned long long total) {
            progress(size/total);
        }];
        [request startSynchronous];
        
        NSError *error=request.error;
        int responseCode=request.responseStatusCode;
        NSString *responseMessage=request.responseStatusMessage;
        
        NSMutableArray *result=nil;
        if (!error) {
            if (responseCode==200) {
                NSString *responseStr=request.responseString;
                
                SBJsonParser *parser=[[[SBJsonParser alloc] init] autorelease];
                id parsedResponse=[parser objectWithString:responseStr error:&error];
                
                if (!error && parsedResponse) {
                    result=[NSMutableArray array];
                    
                    NSArray *twitzInfoList=nil;
                    if ([parsedResponse isKindOfClass:[NSDictionary class]]) {
                        twitzInfoList=[NSArray arrayWithObject:parsedResponse];
                    }else if ([parsedResponse isKindOfClass:[NSArray class]]){
                        twitzInfoList=parsedResponse;
                    }
                    
                    //2012-12-12 17:26:58
                    NSDateFormatter *dateFormatter=[[[NSDateFormatter alloc] init] autorelease];
                    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd' ' HH':'mm':'ss"];
                    for (id item in twitzInfoList) {
                        if ([item isKindOfClass:[NSDictionary class]]) {
                            TwitzInfo *twitz=[[[TwitzInfo alloc] init] autorelease];
                            [result addObject:twitz];
                            twitz.username=[item objectForKey:@"name"];
                            twitz.text=[item objectForKey:@"text"];
                            NSDate *date=[dateFormatter dateFromString:[item objectForKey:@"date"]];
                            twitz.date=date;
                        }
                    }
                }
                

            }
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            if (error || responseCode!=200) {
                NSString *localizedErrorMessage=[error localizedDescription];
                if (!localizedErrorMessage) {
                    localizedErrorMessage=[NSString stringWithFormat:NSLocalizedString(@"Error Code: %d", @""),responseCode];
                    if (responseMessage) {
                        localizedErrorMessage=[localizedErrorMessage stringByAppendingFormat:NSLocalizedString(@", Message: %@", @""),responseMessage];
                    }
                }
                failure(error,localizedErrorMessage);
            }else{
                success(result);
            }

        });
    });
}

@end
